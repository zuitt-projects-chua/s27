//Noode.js Routing with Http methods

/*
Create - post
read - get
update - put/patch
delete - delete

*/

const http = require('http');
//Mock data for users and courses

let users = [
	{
		username: "peterIsHomeless",
		email: "peterParker@mail.com",
		password: "peterNoWayHome"
	},
	{
		username: "Tony3000",
		email: "starksIndustries@gmail.com",
		password: "ironManWillBeBack"
	}
]

let courses = [
	{
		name: "Math 103",
		price: 2500,
		isActive: true
	},
	{
		name: "Biology 201",
		price: 2500,
		isActive: true
	}
]


http.createServer((req, res) =>{
	if(req.url === "/" && req.method === "GET"){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('This route is for checking GET method')
	}else if(req.url === "/" && req.method === "POST"){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('This route is checking a POST method')
	}else if (req.url === "/" && req.method === "PUT"){
		res.writeHead(200, {'Content-Type': 'text-plain'})
		res.end('This route is checking a PUT method')
	}else if (req.url === "/" && req.method === "DELETE"){
		res.writeHead(200, {'Content-Type': 'text-plain'})
		res.end('This route is checking a DELETE method')
	}else if(req.url === "/users" && req.method === "GET"){
		res.writeHead(200, {'Content-Type': 'application/json'})
		res.end(JSON.stringify(users));
	/*
		We cannot pass other data type as a response exept for strings
		to pass the array, stringify first the code as json


	*/
	}else if(req.url === "/courses" && req.method === "GET"){
		res.writeHead(200, {'Content-Type': 'application/json'})
		res.end(JSON.stringify(courses));

	//insert/create new data 
	}else if(req.url === "/users" && req.method === "POST"){
		let requestBody = "";
		/*Receiving data from the client to node js server requires 2 steps
			data step keywrod - thispart will read the stream of data from our client and process the incoming data into the request body variable
		*/
		req.on('data', (data) => {
			console.log(data)
			requestBody += data
		})
		//end step keyword will run once after the request has been completely sent from our clients
		req.on('end', ()=> {
			console.log(requestBody);
			requestBody = JSON.parse(requestBody)
			let newUser = {
				username: requestBody.username,
				email: requestBody.email,
				password: requestBody.password
			}
			users.push(newUser);
			console.log(users);
			res.writeHead(200, {'Content-Type': 'application/json'})
			res.end(JSON.stringify(users));
		})

	}else if(req.url === "/courses" && req.method === "POST"){
		//you can actually use same variable since the scope of let in the statement is not global
		let requestBody2 = "";
		/*Receiving data from the client to node js server requires 2 steps
			data step keywrod - thispart will read the stream of data from our client and process the incoming data into the request body variable
		*/
		req.on('data', (data2) => {
			console.log(data2)
			requestBody2 += data2
		})
		//end step keyword will run once after the request has been completely sent from our clients
		req.on('end', ()=> {
			console.log(requestBody2);
			requestBody2 = JSON.parse(requestBody2)
			let newCourse = {
				name: requestBody2.name,
				price: requestBody2.price,
				isActive: requestBody2.isActive
			}
			courses.push(newCourse);
			console.log(courses);

			//last code
			res.writeHead(200, {'Content-Type': 'application/json'})
			res.end(JSON.stringify(courses));
		})
	}



}).listen(4000);
console.log('Server is running on localhost:4000')




